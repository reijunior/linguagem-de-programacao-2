<?php

function somaBoletos() {
    $march = [
        'Conta 1' => 150,
        'Conta 2' => 129,
        'Conta 3' => 410,
        'Conta 4' => 70,
        'Conta 5' => 39,
        'Conta 6' => 99,
    ];
    $sum = 0;

    foreach ($march as $value)  {
        $sum = $sum + $value;
    };
    echo $sum, "\n";
}

somaBoletos();
?>
