<?php

// function somaBoletos() {
//     $march = [
//         'Conta 1' => 150,
//         'Conta 2' => 129,
//         'Conta 3' => 410,
//         'Conta 4' => 70,
//         'Conta 5' => 39,
//         'Conta 6' => 99,
//     ];
//     $sum = 0;

//     foreach ($march as $value)  {
//         $sum = $sum + $value;
//     };
//     echo $sum, "\n";
// }

// somaBoletos();

// ----------------------------------------------------------------

// class Pessoa {
//     public $nome;

//     function __construct(string $nome) {
//         $this->nome = $nome;
//     }

//     function __toString() : string {
//         return "Pessoa { nome: $this->nome }";
//     }
// }

// $pessoa0 = new Pessoa('Ettore');
// $pessoa1 = new Pessoa('Leandro');

// echo $pessoa0, ' ', $pessoa1, "/n";

// ----------------------------------------------------------------


class Item {
    public string $produto;
    public float $valor;

    public function __construct($produto, $valor) {
        $this->produto = $produto;
        $this->valor = $valor;
    }
}

class Carrinho{

    function __toString():string {
        return "carrinho{valor: $this->valor}";
    }
    public array $items;
    public $valor;

    public function __construct() {
        $this->items = [];
    }

    public function add(Item $item) {
        $this->items[] = $item;
    }

    public function getTotal() : float{
        $total = 0;
        foreach($this->items as $item) {
            $total += $item->valor;
        }
        return $total;
    }

    public function getDescricao() : string {
        $descricao = '';
        foreach($this->items as $item) {
            $descricao .= "$item->produto\n";
        }
        return $descricao;
    }
}

class EmissorMock {

    function __construct() {
    }

    function emitirNfe(Carrinho $carrinho) {
        $total = $carrinho->getTotal();
        $descricao = $carrinho->getDescricao();
        /**
         * codigo muito louco que conversa com o governo
         */
        $nfe = uniqid('nfe-');
        echo $nfe;
        return $nfe;
    }
}

class DbNotas {
    public array $notas;

    // public function __construct() {
    //     $this->notas = [];
    // }
    public function addNfe(EmissorMock $nfe) {
        $this->notas[] = $nfe;
    }

    public function showDb() {
        echo $notas;
        var_dump($notas);
    }
} 

// Criar um objeto para armazenar a nfe, classe nova e usar o emissor mock.
// 

$carrinho = new Carrinho();

$carrinho->add(new Item('Leite', 7.3));
$carrinho->add(new Item('Farinha', 5));

var_dump($carrinho);
// echo $carrinho();

$emissor = new EmissorMock();
$emissor->emitirNfe($carrinho);

$show = new Dbnotas();
$show->showDb();

